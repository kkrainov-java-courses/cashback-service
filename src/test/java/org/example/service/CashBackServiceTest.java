package org.example.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CashBackServiceTest {

    @Test
    void shouldCalculateEqualsMaxCashback() {
        CashBackService cashBackService = new CashBackService();
        int amount = 560780_00;
        int expected = 3000;

        int actual = cashBackService.countCashback(amount);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateLessThenMinBill() {
        CashBackService cashBackService = new CashBackService();
        int amount = 71_00;
        int expected = 0;

        int actual = cashBackService.countCashback(amount);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateLessThenMaxCashback() {
        CashBackService cashBackService = new CashBackService();
        int amount = 31000_00;
        int expected = 310;

        int actual = cashBackService.countCashback(amount);

        assertEquals(expected, actual);
    }

}
