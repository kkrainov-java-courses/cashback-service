package org.example.service;

public class CashBackService {

    public int countCashback(int amount) {
        int initialCashback = 0;
        int maxCashback = 3000;
        int minAmount = 100_00;
        int maxAmount = 300000_00;
        int percent = 1;

        if (amount < minAmount) {
            return initialCashback;
        }

        if (amount >= maxAmount) {
            return maxCashback;
        }

        initialCashback = (amount * percent) / 10_000;
        return  initialCashback;
    }
}
